# define DWM_BARSINGLEBG 0
# define DWM_BORDERPX 4
# define DWM_SNAP 32
# define DWM_SHOWBAR 1
# define DWM_TOPBAR 1
# define DWM_FONT {"IBM Plex Mono", "DejaVu Sans Mono", "monospace", "JoyPixels:size=10:antialias=true:autohint=true", "Symbols Nerd Font:style=2048-em:size=10:antialias=true:autohint=true", "Noto Color Emoji:size=10:style=Regular"}
# define DWM_FOREGROUND "#BBBBBB"
# define DWM_BACKGROUND "#000000"
# define DWM_BORDER "#444444"
# define DWM_SELFOREGROUND "#000000"
# define DWM_SELBACKGROUND "#FF0000"
# define DWM_SELBORDER "#FF0000"
