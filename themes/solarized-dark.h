# define DWM_BARSINGLEBG 1
# define DWM_BORDERPX 6
# define DWM_SNAP 32
# define DWM_SHOWBAR 1
# define DWM_TOPBAR 1
# define DWM_FONT {"IBM Plex Mono", "DejaVu Sans Mono", "monospace", "JoyPixels:size=10:antialias=true:autohint=true", "Symbols Nerd Font:style=2048-em:size=10:antialias=true:autohint=true", "Noto Color Emoji:size=10:style=Regular"}
# define DWM_FOREGROUND "#839496"
# define DWM_BACKGROUND "#002b36"
# define DWM_BORDER "#073642"
# define DWM_SELFOREGROUND "#93a1a1"
# define DWM_SELBACKGROUND "#174956"
# define DWM_SELBORDER "#174956"
